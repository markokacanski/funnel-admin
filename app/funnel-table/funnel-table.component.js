angular.module('myApp').component('funnelTable', {
	templateUrl: "funnel-table/funnel-table.component.html",
	controller: funnelTableController
})

funnelTableController.$inject = ['$scope','funnelStorageService', 'Crosstab']

function funnelTableController($scope, funnelStorageService, Crosstab){
	let vm = this;

	vm.funnels = funnelStorageService.getFunnels();
	Crosstab.listen("FUNNEL_ADDED", (data) => {
		funnelStorageService.addFunnel(data)
		$scope.$apply()
	})
}