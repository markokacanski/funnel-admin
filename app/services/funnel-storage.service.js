angular.module("myApp").service('funnelStorageService', funnelStorageService)

funnelStorageService.$inject = ['$window']

function funnelStorageService($window){
	let rawFunnels = $window.localStorage.getItem('CREATED_FUNNELS') || `[]`
	let existingFunnels = JSON.parse(rawFunnels)
	
	this.getFunnels = () => {
		return existingFunnels
	}

	this.addFunnel = (funnel) =>{
		existingFunnels.push(funnel)
		$window.localStorage.setItem('CREATED_FUNNELS', JSON.stringify(existingFunnels))
	}

}